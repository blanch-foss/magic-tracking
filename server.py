#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
finger tracker for apple (magic) trackpad & magic mouse

Copyright (c) 2011, IIHM/LIG - Renaud Blanch <http://iihm.imag.fr/blanch/>
Licence: GPLv3 or higher <http://www.gnu.org/licenses/gpl.html>
"""


# imports ####################################################################

import sys
import getopt

from os import environ
from time import sleep


# MultitouchSupport ##########################################################

import ctypes
import ctypes.util

# looking up MultitouchSupport into PrivateFrameworks

PrivateFrameworks = "/System/Library/PrivateFrameworks"
try:
	DYLD_FRAMEWORK_PATH = environ["DYLD_FRAMEWORK_PATH"]
except KeyError:
	environ["DYLD_FRAMEWORK_PATH"] = PrivateFrameworks
else:
	environ["DYLD_FRAMEWORK_PATH"] = ":".join([DYLD_FRAMEWORK_PATH,
	                                           PrivateFrameworks])

MT = ctypes.cdll.LoadLibrary(ctypes.util.find_library("MultitouchSupport"))

try:
	environ["DYLD_FRAMEWORK_PATH"] = DYLD_FRAMEWORK_PATH
	del DYLD_FRAMEWORK_PATH
except NameError:
	del environ["DYLD_FRAMEWORK_PATH"]


# structures

class CGPoint(ctypes.Structure):
	_fields_ = [("x", ctypes.c_float), ("y", ctypes.c_float)]

class Touch(ctypes.Structure):
	_fields_ = [("position", CGPoint), ("velocity", CGPoint)]

class Contact(ctypes.Structure):
	_fields_ = [("frame", ctypes.c_int),
	            ("timestamp", ctypes.c_double),
	            ("identifier", ctypes.c_int),
	            ("state", ctypes.c_int),
	            ("unknown1", ctypes.c_int),
	            ("unknown2", ctypes.c_int),
	            ("touch", Touch),
	            ("size", ctypes.c_float),
	            ("unknown3", ctypes.c_int),
	            ("angle", ctypes.c_float),
	            ("major_axis", ctypes.c_float),
	            ("minor_axis", ctypes.c_float),
	            ("unknown4", Touch),
	            ("unknown5", ctypes.c_int),
	            ("unknown6", ctypes.c_int),
	            ("unknown7", ctypes.c_float)]

MTDeviceRef = ctypes.c_void_p
MTContactCallback = ctypes.CFUNCTYPE(ctypes.c_int, *[ctypes.c_uint,
                                                     ctypes.POINTER(Contact),
                                                     ctypes.c_int,
                                                     ctypes.c_double,
                                                     ctypes.c_int])


CFArrayRef = ctypes.c_void_p
CFMutableArrayRef = ctypes.c_void_p
CFIndex = ctypes.c_long


# prototypes

CFArrayGetCount = MT.CFArrayGetCount
CFArrayGetCount.argtypes = [CFArrayRef]
CFArrayGetCount.restype = CFIndex

CFArrayGetValueAtIndex = MT.CFArrayGetValueAtIndex
CFArrayGetValueAtIndex.argtypes = [CFArrayRef, CFIndex]
CFArrayGetValueAtIndex.restype = ctypes.c_void_p


MTDeviceCreateDefault = MT.MTDeviceCreateDefault
MTDeviceCreateDefault.restype = MTDeviceRef
MTDeviceCreateDefault.argtypes = []

MTDeviceCreateList = MT.MTDeviceCreateList
MTDeviceCreateList.restype = CFMutableArrayRef
MTDeviceCreateList.argtypes = []

MTRegisterContactFrameCallback = MT.MTRegisterContactFrameCallback
MTRegisterContactFrameCallback.argtypes = [MTDeviceRef, MTContactCallback]
MTUnregisterContactFrameCallback = MT.MTUnregisterContactFrameCallback
MTUnregisterContactFrameCallback.argtypes = [MTDeviceRef, MTContactCallback]

MTDeviceStart = MT.MTDeviceStart
MTDeviceStart.argtypes = [MTDeviceRef, ctypes.c_int]
MTDeviceStop = MT.MTDeviceStop
MTDeviceStop.argtypes = [MTDeviceRef]
MTDeviceRelease = MT.MTDeviceRelease
MTDeviceRelease.argtypes = [MTDeviceRef]
MTDeviceGetSensorDimensions = MT.MTDeviceGetSensorDimensions
MTDeviceGetSensorDimensions.argtypes = [MTDeviceRef,
                                        ctypes.POINTER(ctypes.c_uint8),
                                        ctypes.POINTER(ctypes.c_uint8)]
MTDeviceGetSensorSurfaceDimensions = MT.MTDeviceGetSensorSurfaceDimensions
MTDeviceGetSensorSurfaceDimensions.argtypes = [MTDeviceRef,
                                               ctypes.POINTER(ctypes.c_short),
                                               ctypes.POINTER(ctypes.c_short)]

#_MTDeviceGetDeviceID
#0000000000002922 T _MTDeviceGetDriverType
#0000000000002c34 T _MTDeviceGetFamilyID


# listing ####################################################################

def get_devices():
	devices = MTDeviceCreateList()
	count = CFArrayGetCount(devices)
	return [CFArrayGetValueAtIndex(devices, i) for i in range(count)]

def listing(out=sys.stdout):
	devices = get_devices()
	for i, device in enumerate(devices):
		w, h = ctypes.c_short(), ctypes.c_short()
		MTDeviceGetSensorSurfaceDimensions(device, ctypes.byref(w), ctypes.byref(h))
		out.write("%s: <0x%x> %.2fx%.2fmm\n" % (i, device,
		                                        w.value/100., h.value/100.))


# callbacks ##################################################################

def make_udp_callback(server):
	"""create raw UDP callback"""
	from socket import socket, AF_INET, SOCK_DGRAM
	magic_socket = socket(AF_INET, SOCK_DGRAM)
	UDP_MESSAGE = "magic %(device)s %(id)s %(state)s %(ts)s %(x)s %(y)s %(size)s %(angle)s %(major)s %(minor)s\n"
	
	@MTContactCallback
	def callback(device, contacts, n_contacts, timestamp, frame):
		lines = []
		for contact in contacts[:n_contacts]:
			lines.append(UDP_MESSAGE % {
				"device": device,
				"id":     contact.identifier,
				"state":  contact.state,
				"ts":     contact.timestamp,
				"x":      contact.touch.position.x,
				"y":      contact.touch.position.y,
				"size":   contact.size,
				"angle":  contact.angle,
				"major":  contact.major_axis,
				"minor":  contact.minor_axis,
			})
		magic_socket.sendto("".join(lines), server)
		return 0
	return callback


def make_osc_callback(server):
	"""create OSC callback"""
	from OSC import OSCClient, OSCClientError, OSCMessage, OSCBundle
	
	OSC_ADDRESS = "/magic"
	
	osc_client = OSCClient()
	osc_client.connect(server)

	@MTContactCallback
	def callback(device, contacts, n_contacts, timestamp, frame):
		b = OSCBundle()
		for contact in contacts[:n_contacts]:
			m = OSCMessage(OSC_ADDRESS)
			m.append(device)
			m.append(contact.identifier)
			m.append(contact.state)
			m.append(contact.timestamp)
			m.append(contact.touch.position.x)
			m.append(contact.touch.position.y)
			m.append(contact.size)
			m.append(contact.angle)
			m.append(contact.minor_axis)
			m.append(contact.major_axis)
			b.append(m)
		try:
			osc_client.send(b)
		except OSCClientError:
			return -1
		return 0
	return callback


CALLBACK_MAKERS = {
	"udp": make_udp_callback,
	"osc": make_osc_callback,
}


# server #####################################################################

def serve(callback, *indices):
	devices = get_devices()
	if indices:
		devices = [devices[int(i)] for i in indices]
	
	for device in devices:
		MTRegisterContactFrameCallback(device, callback)
		MTDeviceStart(device, 0)
	
	while True:
		try:
			sleep(1)
		except KeyboardInterrupt:
			break
	
	for device in devices:
		MTUnregisterContactFrameCallback(device, callback)
		MTDeviceStop(device)
		MTDeviceRelease(device)


# main #######################################################################

DEFAULT_HOST         = "localhost"
DEFAULT_PORT         = 3333
DEFAULT_MESSAGE_TYPE = "udp"

def exit_usage(name, message=None, code=0):
	from textwrap import dedent
	usage = dedent("""\
	Usage: %s [-lh:p:t:] [<d0>, ...]
		-l --list         print the number of trackable devices, then exit
		-h --host <host>  target host (defaults to "%s")
		-p --port <port>  target port (defaults to %s)
		-t --type <type>  message type ("udp"|"osc", defaults to "%s")
		<d0>, ...         devices to track (all device if none given)
	""")
	if message:
		sys.stderr.write("%s\n" % message)
	sys.stderr.write(usage % (name, DEFAULT_HOST, DEFAULT_PORT, DEFAULT_MESSAGE_TYPE))
	sys.exit(code)


def main(argv=None):
	if argv is None:
		argv = sys.argv
	name, args = argv[0], argv[1:]
	
	try:
		opts, args = getopt.getopt(args, "lh:p:t:", ["list", "host=", "port=", "type="])
	except getopt.GetoptError, message:
		exit_usage(name, message, 1)
	
	host         = DEFAULT_HOST
	port         = DEFAULT_PORT
	message_type = DEFAULT_MESSAGE_TYPE
	
	for opt, value in opts:
		if opt in ["-l", "--list"]:
			listing()
			sys.exit()
		elif opt in ["-h", "--host"]:
			host = value
		elif opt in ["-p", "--port"]:
			port = int(value)
		elif opt in ["-t", "--type"]:
			message_type = value
	
	try:
		callback_maker = CALLBACK_MAKERS[message_type]
	except KeyError:
		exit_usage(name, 'unknown message type "%s"' % message_type, 1)
		
	serve(callback_maker((host, port)), *args)
	

if __name__ == "__main__":
	sys.exit(main())
