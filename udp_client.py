#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
sample udp client for magic tracker

Copyright (c) 2011, IIHM/LIG - Renaud Blanch <http://iihm.imag.fr/blanch/>
Licence: GPLv3 or higher <http://www.gnu.org/licenses/gpl.html>
"""

# imports ####################################################################

import sys
import getopt


# listening ##################################################################

def listen(server):
	from socket import socket, AF_INET, SOCK_DGRAM
	MAX_LEN = 1024
	
	magic_socket = socket(AF_INET, SOCK_DGRAM)
	magic_socket.bind(server)
	
	while True:
		try:
			msg = magic_socket.recv(MAX_LEN)
		except KeyboardInterrupt:
			break
		for line in msg.split("\n"):
			if not line:
				continue
			(magic, device_id, contact_id, state, timestamp, 
			 x, y, size, angle, major, minor) = line.split()
			assert magic == "magic"
			print device_id, contact_id, x, y


# main #######################################################################

DEFAULT_HOST = "localhost"
DEFAULT_PORT = 3333

def exit_usage(name, message=None, code=0):
	from textwrap import dedent
	usage = dedent("""\
	Usage: %s [-h:p:]
		-h --host <host>  target host (defaults to "%s")
		-p --port <port>  target port (defaults to %s)
	""")
	if message:
		sys.stderr.write("%s\n" % message)
	sys.stderr.write(usage % (name, DEFAULT_HOST, DEFAULT_PORT))
	sys.exit(code)


def main(argv=None):
	if argv is None:
		argv = sys.argv
	name, args = argv[0], argv[1:]
	
	try:
		opts, args = getopt.getopt(args, "h:p:", ["host=", "port="])
	except getopt.GetoptError, message:
		exit_usage(name, message, 1)
	
	host = DEFAULT_HOST
	port = DEFAULT_PORT
	
	for opt, value in opts:
		if opt in ["-h", "--host"]:
			host = value
		elif opt in ["-p", "--port"]:
			port = int(value)
	
	listen((host, port))
		

if __name__ == "__main__":
	sys.exit(main())
